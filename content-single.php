<?php
/**
 * @package sparkling
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php the_post_thumbnail('sparkling-featured', array('class' => 'single-featured')); ?>
    <div class="post-inner-content">
        <div class="row">
            <div class="col-sm-2">
                <div class="secondary-content-box">
                    <!-- author bio -->
                    <div class="author-bio content-box-inner">

                        <!-- avatar -->
                        <div class="avatar">
                            <?php 
                            $post_user_ID = get_the_author_meta('ID');
                            echo get_avatar($post_user_ID, '60'); 
                            $ramura_varsta = get_user_meta( $post_user_ID, 'ramura_varsta',true ); 
                            echo $ramura_varsta;
                            ?>
                        </div>
                        <!-- end avatar -->

                        <!-- user bio -->
                        <div class="author-bio-content">

                            <h4 class="author-name"><a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php echo get_the_author_meta('display_name'); ?></a></h4>
                            <p class="author-description">
                                <?php echo get_the_author_meta('description'); ?>
                            </p>

                        </div>
                        <!-- end author bio -->

                    </div>
                    <!-- end author bio -->
                </div>
            </div>
            <div class="col-sm-10">
                <header class="entry-header page-header">


                    <h1 class="entry-title "><?php the_title(); ?></h1>

                    <div class="entry-meta">
                        <?php sparkling_posted_on(); ?>

                        <?php
                        /* translators: used between list items, there is a space after the comma */
                        $categories_list = get_the_category_list(__(', ', 'sparkling'));
                        if ($categories_list && sparkling_categorized_blog()) :
                            ?>
                            <span class="cat-links"><i class="fa fa-folder-open-o"></i>
                                <?php printf(__(' %1$s', 'sparkling'), $categories_list); ?>
                            </span>
                        <?php endif; // End if categories ?>
                        <?php edit_post_link(__('Edit', 'sparkling'), '<i class="fa fa-pencil-square-o"></i><span class="edit-link">', '</span>'); ?>

                    </div><!-- .entry-meta -->
                </header><!-- .entry-header -->

                <div class="entry-content">
                    <?php the_content(); ?>
                    <?php
                    wp_link_pages(array(
                        'before' => '<div class="page-links">' . __('Pages:', 'sparkling'),
                        'after' => '</div>',
                        'link_before' => '<span>',
                        'link_after' => '</span>',
                        'pagelink' => '%',
                        'echo' => 1
                    ));
                    ?>
                </div><!-- .entry-content -->
            </div>
        </div>


        <footer class="entry-meta">

            <?php if (has_tag()) : ?>
                <!-- tags -->
                <div class="tagcloud">

                    <?php
                    $tags = get_the_tags(get_the_ID());
                    foreach ($tags as $tag) {
                        echo '<a href="' . get_tag_link($tag->term_id) . '">' . $tag->name . '</a> ';
                    }
                    ?>

                </div>
                <!-- end tags -->
            <?php endif; ?>

        </footer><!-- .entry-meta -->
    </div>

</article><!-- #post-## -->