<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package sparkling
 */
?><!doctype html>
<!--[if !IE]>
<html class="no-js non-ie" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>
<html class="no-js ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>
<html class="no-js ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>
<html class="no-js ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

        <?php wp_head(); ?>

    </head>

    <body <?php body_class(); ?>>
        <div id="page" class="hfeed site">

            <header id="masthead" class="site-header" role="banner">
                <nav class="navbar navbar-default" role="navigation">
                    <div class="container">
                        <div class="row">
                            <div class="site-navigation-inner col-sm-12">
                                <div class="navbar-header">
                                    <button type="button" class="btn navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>

                                    <?php if (get_header_image() != '') : ?>

                                        <div id="logo">
                                            <a href="<?php echo esc_url(home_url('/')); ?>"><img src="<?php header_image(); ?>"  height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" alt="<?php bloginfo('name'); ?>"/></a>
                                        </div><!-- end of #logo -->
                                        <div id="oncr"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/oncr.png" alt="ONCR" /></a></div>

                                    <?php endif; // header image was removed ?>

                                    <?php if (!get_header_image()) : ?>

                                        <div id="logo">
                                            <span class="site-name"><a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home"><?php bloginfo('name'); ?></a></span>
                                        </div><!-- end of #logo -->

                                    <?php endif; // header image was removed (again) ?>

                                </div>
                                <?php sparkling_header_menu(); ?>
                            </div>
                        </div>
                    </div>
                </nav><!-- .site-navigation -->
            </header><!-- #masthead -->

            <div id="content" class="site-content">

                <div class="top-section">
                    <?php sparkling_featured_slider(); ?>
                    <?php sparkling_call_for_action(); ?>
                </div>

                <div class="container main-content-area">
                    <?php
                    if (is_front_page()) {
                        $home_category = of_get_option('home_category');





                        $args = array(
                            'cat' => $home_category,
                            'posts_per_page' => 4
                        );
                        // The Query
                        $the_query = new WP_Query($args);

// The Loop
                        $posts_array = array();
                        if ($the_query->have_posts()) {
                            while ($the_query->have_posts()) {
                                $the_query->the_post();
                                $post_id = get_the_ID();
                                //
                                $posts_array[] = array(
                                    'title' => get_the_title(),
                                    'image' => get_the_post_thumbnail($post_id, 'as-grid'),
                                    'image_small' => get_the_post_thumbnail($post_id, 'as-grid-small'),
                                    'link' => get_the_permalink()
                                );
                            }
                        }

                        /* Restore original Post Data */
                        wp_reset_postdata();
                        ?>
                        <div class="row custom-grid">
                            <div class="col-sm-12">
                                <div class="large-part">
                                    <a href="<?php echo $posts_array[1]['link']; ?>"><div class="title"><?php echo $posts_array[0]['title']; ?></div><?php echo $posts_array[0]['image']; ?></a>
                                </div>
                                <div class="small-part">
                                    <div class="custom-row">
                                        <a href="<?php echo $posts_array[1]['link']; ?>"><div class="title"><?php echo $posts_array[1]['title']; ?></div><?php echo $posts_array[1]['image_small']; ?></a>
                                    </div>
                                    <div class="custom-row">
                                        <a href="<?php echo $posts_array[1]['link']; ?>"><div class="title"><?php echo $posts_array[2]['title']; ?></div><?php echo $posts_array[2]['image_small']; ?></a>
                                    </div>
                                    <div class="custom-row">
                                        <a href="<?php echo $posts_array[1]['link']; ?>"><div class="title"><?php echo $posts_array[3]['title']; ?></div><?php echo $posts_array[3]['image_small']; ?></a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    <?php } ?>
                    <div class="row">
                        <div class="main-content-inner <?php echo sparkling_main_content_bootstrap_classes(); ?> <?php echo of_get_option('site_layout'); ?>">
